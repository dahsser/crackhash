import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { LoginPage, MainPage } from './pages/';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div id="root">
      <CssBaseline />
      <Router>
        <Switch>
          <Route exact path="/">
            <LoginPage />
          </Route>
          <Route exact path="/main">
            <MainPage />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
