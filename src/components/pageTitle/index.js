import React from 'react';
import { Typography } from '@material-ui/core';

const PageTitle = ({ title = 'SnapIoT', variant = 'h3', ...props }) => {
    return (<Typography component="h1" variant={variant} style={{ textAlign: "center" }} {...props}>{title}</Typography>)
}

export default PageTitle;