import React, { useState } from 'react';
import { Container, Button, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Title } from '../../components';
import axios from 'axios';
const useStyles = makeStyles(theme => ({
	container: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	textField: {
		width: "100%",
	},
}));

const MainPage = () => {
	const [valueHash, setValueHash] = useState("");
	const [crackedHash, setCrackedHash] = useState("");
	const classes = useStyles();
	const handleOnClick = () => {
		alert("Crackeando tu hash MD5, un momento por favor...")
		axios.get(`https://api.raspberry-01.dahsser.com/crackhash/${valueHash}`)
			.then(function (response) {
				// handle success
				console.log(response.data);
				alert("El servidor ha respondido!")
				setCrackedHash(response.data.original)
			})
	}
	return (
		<Container maxWidth="xl" style={{ backgroundColor: '#eee', height: '100vh' }} >
			<div style={{ paddingTop: "20px" }}>
				<Title />
			</div>
			<div style={{ maxWidth: "800px", margin: "25px auto" }}>
				<TextField
					id="standard-basic"
					value={valueHash}
					onChange={(e) => setValueHash(e.target.value)}
					className={classes.textField}
					label="Inserta tu HASH MD5"
					margin="normal"
				/>
			</div>
			<div style={{ margin: "0 auto", display: "flex", justifyContent: "center" }}>
				<Button variant="contained" className={classes.button} style={{ marginRight: "10px" }}>
					Borrar
                </Button>
				<Button variant="contained" color="primary" className={classes.button} onClick={handleOnClick}>
					Romper Hash
				</Button>
				
			</div>
			<div style={{margin: "30px auto", width: "500px"}}><strong>Cracked hash:</strong> {crackedHash}</div>

		</Container>
	)
}

export default MainPage;
